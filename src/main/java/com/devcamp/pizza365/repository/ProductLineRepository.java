package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.ProductLine;

public interface ProductLineRepository extends JpaRepository<ProductLine, Integer>{
    
}
