package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer>{
    
}
