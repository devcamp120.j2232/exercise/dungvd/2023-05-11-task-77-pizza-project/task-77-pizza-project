package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
    
}
