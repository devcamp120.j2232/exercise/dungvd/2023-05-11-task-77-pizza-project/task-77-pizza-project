package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Office;

public interface OfficeRepository extends JpaRepository<Office, Integer>{
    
}
