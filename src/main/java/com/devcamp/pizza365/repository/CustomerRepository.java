package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Customer;

public interface CustomerRepository extends JpaRepository <Customer, Integer>{
    @Query(value = "DELETE FROM customers WHERE id = :customerId", nativeQuery = true)
    void deleteCustomerById(@Param("customerId") Integer customerId);
}
