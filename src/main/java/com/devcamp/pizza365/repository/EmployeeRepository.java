package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
    
}
