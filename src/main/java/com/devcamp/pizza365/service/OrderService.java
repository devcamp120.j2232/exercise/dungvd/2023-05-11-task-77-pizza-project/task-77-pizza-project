package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;

@Service
public class OrderService {
    @Autowired
    OrderRepository iOrderRepository;

    @Autowired
    CustomerRepository iCustomerRepository;

    // Hàm lấy ra danh sách toàn bộ oder
    public List<Order> getAllOrders() {
        List<Order> listOrders = new ArrayList<>();
        iOrderRepository.findAll().forEach(listOrders::add);
        return listOrders;
    }

    // Hàm tạo mới 1 Order
    public Order createOrder(Integer customerId, Order pOrder) {
        Customer customer = iCustomerRepository.findById(customerId).get();
        Date date = new Date();
        pOrder.setCustomer(customer);
        pOrder.setOrderDate(date);
        pOrder.setRequiredDate(date);
        Order orderCreated = iOrderRepository.save(pOrder);
        return orderCreated;
    }

    // Hàm update 1 order
    public Order updateOrder(Integer customerId, Integer orderId, Order pOrder) {
        Customer customer = iCustomerRepository.findById(customerId).get();
        Order order = iOrderRepository.findById(orderId).get();
        order.setComments(pOrder.getComments());
        order.setCustomer(customer);
        order.setShippedDate(pOrder.getShippedDate());
        order.setStatus(pOrder.getStatus());
        order.setRequiredDate(pOrder.getRequiredDate());
        order.setShippedDate(pOrder.getShippedDate());

        Order orderUpdated = iOrderRepository.save(order);
        return orderUpdated;
    }

    // Hàm xóa 1 orrder
    public void deleteOrder(Integer orderId) {
        iOrderRepository.deleteById(orderId);
    }
}
