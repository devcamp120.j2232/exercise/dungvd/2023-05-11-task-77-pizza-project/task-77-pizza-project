package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.ProductLineRepository;
import com.devcamp.pizza365.repository.ProductRepository;

@Service
public class ProductService {
    @Autowired
    ProductRepository iProductRepository;

    @Autowired
    ProductLineRepository iProductLineRepository;

    // Hàm lấy ra danh sách toàn bộ product
    public List<Product> getAllProducts() {
        List<Product> listProducts = new ArrayList<>();
        iProductRepository.findAll().forEach(listProducts::add);
        return listProducts;
    }

    // Hàm tạo mới 1 product
    public Product createProduct(Integer productLineId, Product pProduct) {
        ProductLine productLine = iProductLineRepository.findById(productLineId).get();
        pProduct.setProductLine(productLine);
        Product productCreated = iProductRepository.save(pProduct);
        return productCreated;
    }

    // Hàm update 1 product
    public Product updateProduct(Integer productLineId, Integer productId, Product pProduct) {
        ProductLine productLine = iProductLineRepository.findById(productLineId).get();
        Product product = iProductRepository.findById(productId).get();
        product.setBuyPrice(pProduct.getBuyPrice());
        product.setProductLine(productLine);
        product.setProductScale(pProduct.getProductScale());
        product.setProductCode(pProduct.getProductCode());
        product.setProductDescripttion(pProduct.getProductDescripttion());
        product.setProductVendor(pProduct.getProductVendor());
        product.setQuantityInStock(pProduct.getQuantityInStock());
        product.setProductCode(pProduct.getProductCode());

        Product productUpdated = iProductRepository.save(product);
        return productUpdated;
    }

    // Hàm xóa 1 product
    public void deleteProduct(Integer productId) {
        iProductRepository.deleteById(productId);
    }
}
