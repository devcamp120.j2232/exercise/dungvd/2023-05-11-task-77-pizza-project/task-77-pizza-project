package com.devcamp.pizza365.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository iCustomerRepository;

    // Hàm cập mới customer 
    public Customer updateCustomer(Customer pCustomer, Integer id) {
        Customer customer = iCustomerRepository.findById(id).get();
        customer.setLastName(pCustomer.getLastName());
        customer.setFirstName(pCustomer.getFirstName());
        customer.setPhoneNumber(pCustomer.getPhoneNumber());
        customer.setAddress(pCustomer.getAddress());
        customer.setCity(pCustomer.getCity());
        customer.setState(pCustomer.getState());
        customer.setPostalCode(pCustomer.getPostalCode());
        customer.setCountry(pCustomer.getCountry());
        customer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
        customer.setCreditLimit(pCustomer.getCreditLimit());
        Customer customerUpdated = iCustomerRepository.save(customer);
        return customerUpdated;
    }
}
