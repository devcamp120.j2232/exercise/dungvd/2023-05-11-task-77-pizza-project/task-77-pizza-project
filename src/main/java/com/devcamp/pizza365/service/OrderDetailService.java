package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.OrderDetailRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.repository.ProductRepository;

@Service
public class OrderDetailService {
    @Autowired
    OrderDetailRepository pOrderDetailRepository;

    @Autowired
    OrderRepository pOrderRepository;

    @Autowired
    ProductRepository iProductRepository;
    // Hàm lấy ra toàn bộ order detail
    public List<OrderDetail> getAllOrderDetail() {
        List<OrderDetail> listAllOrderDetail = new ArrayList<>();
        pOrderDetailRepository.findAll().forEach(listAllOrderDetail::add);
        return listAllOrderDetail;
    }

    // Hàm tạo mới một order detail
    public OrderDetail createOrderDetail(OrderDetail pOrderDetail, Integer OrderId, Integer productId) {
        Order vOrder = pOrderRepository.findById(OrderId).get();
        Product vProduct = iProductRepository.findById(productId).get();
        
        pOrderDetail.setOrder(vOrder);
        pOrderDetail.setProduct(vProduct);
        OrderDetail createdOrderDetail = pOrderDetailRepository.save(pOrderDetail);
        return createdOrderDetail;
    }

    // Hàm sửa 1 order detail
    public OrderDetail updateOrderDetail(OrderDetail pOrderDetail, Integer orderId, Integer OrderDetailId, Integer productId) {
        Order vOrder = pOrderRepository.findById(orderId).get();
        OrderDetail vOrderDetail = pOrderDetailRepository.findById(OrderDetailId).get();
        Product vProduct = iProductRepository.findById(productId).get();
        vOrderDetail.setOrder(vOrder);
        vOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
        vOrderDetail.setProduct(vProduct);
        vOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
        OrderDetail updatedOrderDetail = pOrderDetailRepository.save(vOrderDetail);
        return updatedOrderDetail;
    }


    // Hàm xóa 1 order detail
    public void deleteOrderDetail(Integer orderDetailId) {
        pOrderDetailRepository.deleteById(orderDetailId);
    }
}
