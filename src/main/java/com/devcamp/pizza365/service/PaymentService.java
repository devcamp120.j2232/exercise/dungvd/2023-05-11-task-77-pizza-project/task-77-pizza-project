package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.PaymentRepository;

@Service
public class PaymentService {
    @Autowired
    PaymentRepository iPaymentRepository;

    @Autowired
    CustomerRepository iCustomerRepository;

    // Hàm lấy ra danh sách toàn bộ payment
    public Object getAllPayments() {
        List<Payment> listPayments = new ArrayList<Payment>();
        iPaymentRepository.findAll().forEach(listPayments::add);
        return listPayments;
    }

    // Hàm tạo mới 1 payment
    public Payment createPayment(Integer customerId, Payment pPayment) {
        Date newDate = new Date();
        Customer customer = iCustomerRepository.findById(customerId).get();
        pPayment.setCustomer(customer);
        pPayment.setPaymentDate(newDate);
        Payment paymentCreated = iPaymentRepository.save(pPayment);
        return paymentCreated;
    }

    // Hàm update 1 payment
    public Payment updatePayment(Integer customerId, Integer paymentId, Payment pPayment) {
        Customer customer = iCustomerRepository.findById(customerId).get();
        Payment payment = iPaymentRepository.findById(paymentId).get();
        payment.setAmmount(pPayment.getAmmount());
        payment.setCheckNumber(pPayment.getCheckNumber());
        payment.setCustomer(customer);
        Payment paymentUpdated = iPaymentRepository.save(payment);
        return paymentUpdated;
    }

    // Hàm xóa 1 payment
    public void deletePayment(Integer paymentId) {
        iPaymentRepository.deleteById(paymentId);
    }
}
