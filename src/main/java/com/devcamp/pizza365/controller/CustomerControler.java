package com.devcamp.pizza365.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerControler {
    @Autowired
    CustomerRepository iCustomerRepository;

    @Autowired
    CustomerService customerService;
    // APi lấy ra toàn bộ danh sách customer
    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomer() {
        try {
            return new ResponseEntity<>(iCustomerRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm tạo mới 1 customer 
    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer pCustomer) {
        try {
            return new ResponseEntity<>(iCustomerRepository.save(pCustomer), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    // Cập nhật thông tin 1 customer
    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@RequestBody Customer pCustomer, @PathVariable("id") Integer id) {
        try {
            Customer customerUpdated = customerService.updateCustomer(pCustomer, id);
            return new ResponseEntity<>(customerUpdated, HttpStatus.OK);
        } catch (Exception e) {
            System.out.print(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 customer
    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable("customerId") Integer customerId) {
        try {
            iCustomerRepository.deleteById(customerId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.print(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
