package com.devcamp.pizza365.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.service.PaymentService;

@RestController
@CrossOrigin
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @Autowired
    CustomerRepository iCustomerRepository;

    // API lấy ra toàn bộ payment
    @GetMapping("/payments")
    public ResponseEntity<Object> getAllPayments() {
        try {
            Object listPayments = paymentService.getAllPayments();
            return new ResponseEntity<>(listPayments, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 payment
    @PostMapping("/customers/{customerId}/payments") 
    public ResponseEntity<Payment> createPayment(@RequestBody Payment pPayment,@PathVariable("customerId") Integer customerId){
        try {
            Payment paymentCreated = paymentService.createPayment(customerId, pPayment);
            return new ResponseEntity<>(paymentCreated, HttpStatus.OK);
        
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật thông tin 1 payment
    @PutMapping("/customer/{customerId}/payments/{paymentId}")
    public ResponseEntity<Payment> updatePayment(@RequestBody Payment pPayment,@PathVariable("customerId") Integer customerId, @PathVariable("paymentId") Integer paymentId) {
        try {
            Payment paymentUpdated = paymentService.updatePayment(customerId, paymentId, pPayment);
            return new ResponseEntity<>(paymentUpdated, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // APi xóa 1 payment
    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Payment> deletePayment(@PathVariable("id") Integer id) {
        try {
            paymentService.deletePayment(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    // API lấy ra list payment của 1 khách hàng
    @GetMapping("/customers/{id}/payments")
    public ResponseEntity<Object> getListPaymentByCustomerId(@PathVariable("id") int customerId) {
        try {
            Customer customer = iCustomerRepository.findById(customerId).get();
            return new ResponseEntity<>(customer.getPayments(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
