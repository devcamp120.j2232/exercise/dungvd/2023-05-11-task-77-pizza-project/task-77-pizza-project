package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.service.OfficeService;

@RestController
@CrossOrigin
public class OfficeController {
    @Autowired
    OfficeService iOfficeService;

    // API lấy ra toàn bộ offices
    @GetMapping("/offices")
    public ResponseEntity<Object> getAllOffices() {
        try {
            List<Office> allEmployees = iOfficeService.getAllOffices();
            return new ResponseEntity<>(allEmployees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 offices
    @PostMapping("/offices")
    public ResponseEntity<Office> createOffice( @RequestBody Office pOffice) {
        try {
            Office createdOffice = iOfficeService.createOffice(pOffice);
            return new ResponseEntity<>(createdOffice, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 office
    @PutMapping("/offices/{id}") 
    public ResponseEntity<Office> updateOffices(@PathVariable("id") Integer id, 
                                                    @RequestBody Office pOffice) {
        try {
            Office updatedOffice = iOfficeService.updateOffice(id,pOffice);
            return new ResponseEntity<>(updatedOffice, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 product
    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Office> deleteOffice(@PathVariable("id") Integer id) {
        try {
            iOfficeService.deleteOffice(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
}
