package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.repository.OrderDetailRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.service.OrderDetailService;

@RestController
@CrossOrigin
public class OrderDetailController {
    @Autowired
    OrderDetailService orderDetailService;


    @Autowired
    OrderRepository iOrderRepository;
    // API lấy ra toàn bộ order detail
    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
        try {
            List<OrderDetail> allOrderDetails = orderDetailService.getAllOrderDetail();
            return new ResponseEntity<>(allOrderDetails, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 order detail
    @PostMapping("/orders/{id}/products/{productId}/orderdetails")
    public ResponseEntity<OrderDetail> createNewOrderDetail(@PathVariable("id") Integer id, @RequestBody OrderDetail pOrderDetail, @PathVariable("productId") Integer productId) {
        try {
            OrderDetail createdOrderDetail = orderDetailService.createOrderDetail(pOrderDetail, id, productId);
            return new ResponseEntity<>(createdOrderDetail, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 order detail
    @PutMapping("/orders/{orderId}/products/{productId}/orderdetails/{orderDetailId}") 
    public ResponseEntity<OrderDetail> updateOrderDetail(@PathVariable("orderId") Integer orderId, 
                                                        @PathVariable("productId") Integer productId, 
                                                        @PathVariable("orderDetailId") Integer orderDetailId, 
                                                        @RequestBody OrderDetail pOrderDetail) {
        try {
            OrderDetail updatedOrderDetail = orderDetailService.updateOrderDetail(pOrderDetail, orderId, orderDetailId, productId);
            return new ResponseEntity<>(updatedOrderDetail, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 order detail
    @DeleteMapping("/orderdetails/{id}")
    public ResponseEntity<OrderDetail> deleteOrderDetail(@PathVariable("id") Integer id) {
        try {
            orderDetailService.deleteOrderDetail(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }   

     // API lấy ra list payment của 1 khách hàng
     @GetMapping("/orders/{id}/orderdetails")
     public ResponseEntity<Object> getListOrderDetailByOrderId(@PathVariable("id") int orderId) {
         try {
             Order order = iOrderRepository.findById(orderId).get();
             return new ResponseEntity<>(order.getOrderDetails(), HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
}
