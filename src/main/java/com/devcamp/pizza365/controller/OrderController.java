package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService orderService;
    
    @Autowired
    CustomerRepository iCustomerRepository;

    @Autowired
    OrderRepository iOrderRepository;
    // API lấy ra toàn bộ order
    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrder() {
        try {
            return new ResponseEntity<>(iOrderRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm tạo mới 1 orders
    @PostMapping("/customers/{customerId}/orders")
    public ResponseEntity<Object> createOrder(@RequestBody Order pOrder, @PathVariable("customerId") Integer customerId) {
        try {
            return new ResponseEntity<>(orderService.createOrder(customerId, pOrder), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    // Cập nhật thông tin 1 order
    @PutMapping("/customers/{customerId}/orders/{orderId}")
    public ResponseEntity<Object> updateOrder(@RequestBody Order pOrder, @PathVariable("customerId") Integer customerId, @PathVariable("orderId") Integer orderId) {
        try {
            return new ResponseEntity<>(orderService.updateOrder(customerId, orderId, pOrder), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 orders
    @DeleteMapping("/orderss/{id}")
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") Integer id) {
        try {
            orderService.deleteOrder(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm lấy toàn bộ order thông qua customer id
    @GetMapping("/customers/{id}/orders")
    public ResponseEntity<Object> getOrderByCustomerId(@PathVariable("id") Integer id) {
        try {
            Customer vCustomer = iCustomerRepository.findById(id).get();
            return new ResponseEntity<>( vCustomer.getOrders(), HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
