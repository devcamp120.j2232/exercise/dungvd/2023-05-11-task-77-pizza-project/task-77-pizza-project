package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.service.ProductService;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductService productService;



    // API lấy ra toàn bộ product
    @GetMapping("/products")
    public ResponseEntity<Object> getAllProduct() {
        try {
            List<Product> allProducts = productService.getAllProducts();
            return new ResponseEntity<>(allProducts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 product
    @PostMapping("/productlines/{id}/products")
    public ResponseEntity<Product> createNewProduct(@PathVariable("id") Integer id, @RequestBody Product pProduct) {
        try {
            Product createdProduct = productService.createProduct(id, pProduct);
            return new ResponseEntity<>(createdProduct, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 product
    @PutMapping("/productlines/{productlineId}/products/{productId}") 
    public ResponseEntity<Product> updateProduct(@PathVariable("productlineId") Integer productlineId, 
                                                        @PathVariable("productId") Integer productId, 
                                                        @RequestBody Product pProduct) {
        try {
            Product updatedProduct = productService.updateProduct(productlineId, productId, pProduct);
            return new ResponseEntity<>(updatedProduct, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 product
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") Integer id) {
        try {
            productService.deleteProduct(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
}
